# APNSNoPack

This is a MobileSubstrate tweak that makes apsd
(the push notification daemon on iOS)
use the older network protocol,
instead of the new "apns-pack-v1".
This is needed because my [APNS Wireshark dissector](https://gitlab.com/nicolas17/apns-dissector/)
doesn't yet support the packed protocol.

Only tested on iOS 12.4.8 and 13.7.
