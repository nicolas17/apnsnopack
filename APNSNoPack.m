// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <CydiaSubstrate/CydiaSubstrate.h>
#include <Foundation/Foundation.h>

#include <os/log.h>
#include <objc/objc.h>

BOOL (*origStreamSetProperty)(
    id self, SEL _cmd,
    id property,
    NSString* key
);
BOOL myStreamSetProperty(
    id self, SEL _cmd,
    id property,
    NSString* key
) {
    if ([key isEqualToString:@"_kCFStreamPropertyNPNProtocolsAvailable"] &&
        [property isKindOfClass:[NSArray class]] &&
        [property isEqualToArray:@[@"apns-security-v3", @"apns-pack-v1"]])
    {
        os_log(OS_LOG_DEFAULT, "Tweak: CFStream setProperty:forKey:NPNProtocolsAvailable called, replacing protocol list");
        property = @[@"apns-security-v2", @"apns-security-v3"];
    }
    return origStreamSetProperty(self, _cmd, property, key);
}
void (*origSetSocketStreamProps)(
    id self, SEL _cmd,
    NSDictionary* properties
);
static void mySetSocketStreamProps(
    id self, SEL _cmd,
    NSMutableDictionary* properties
) {
    // assuming properties is a *Mutable* Dictionary
    os_log(OS_LOG_DEFAULT, "Tweak: NSURLSessionConfiguration set_socketStreamProperties called");
    id property = [properties objectForKey:@"_kCFStreamPropertyNPNProtocolsAvailable"];
    if (property &&
        [property isKindOfClass:[NSArray class]] &&
        [property isEqualToArray:@[@"apns-security-v3", @"apns-pack-v1"]])
    {
        os_log(OS_LOG_DEFAULT, "Tweak: property found, replacing value");
        [properties setObject:@[@"apns-security-v2", @"apns-security-v3"] forKey:@"_kCFStreamPropertyNPNProtocolsAvailable"];
    }
    origSetSocketStreamProps(self, _cmd, properties);
}

MSInitialize {
    os_log(OS_LOG_DEFAULT, "APSTweak: loading");

    // iOS 12
    Class streamClass = objc_getClass("__NSCFInputStream");
    if (streamClass) {
        os_log(OS_LOG_DEFAULT, "Tweak: stream class found, hooking");
        MSHookMessageEx(
            streamClass,
            @selector(setProperty:forKey:),
            (IMP)&myStreamSetProperty,
            (IMP*)&origStreamSetProperty
        );
    }

    // iOS 13
    Class cfUrlSessionConfClass = objc_getClass("__NSCFURLSessionConfiguration_Mutable");
    if (cfUrlSessionConfClass) {
        os_log(OS_LOG_DEFAULT, "Tweak: CF session configuration class found, hooking");
        MSHookMessageEx(
            cfUrlSessionConfClass,
            @selector(set_socketStreamProperties:),
            (IMP)&mySetSocketStreamProps,
            (IMP*)&origSetSocketStreamProps
        );
    } else {
        // iOS 14
        Class urlSessionConfClass = objc_getClass("NSURLSessionConfiguration");
        if (urlSessionConfClass) {
            os_log(OS_LOG_DEFAULT, "Tweak: Session configuration class found, hooking");
            MSHookMessageEx(
                urlSessionConfClass,
                @selector(set_socketStreamProperties:),
                (IMP)&mySetSocketStreamProps,
                (IMP*)&origSetSocketStreamProps
            );
        }
    }
}
